function conf = getDefaultConfForPlotByLayers()
% Return a default conf structure for the function plotByLayers()

% Author
%
% Yaoyu HU <huyaoyu@sjtu.edu.cn>

% Modified on 2015-10-17
% Support semilog plot by adding a new field in the conf struct.
% The new field is plotType
%
% Modified on 2016-07-29
% Add working directory. Figure will be saved into the working directory.
% 
% Modified on 2016-08-08
% Add interpreter for legend. Add grid switch flag.
% 


conf.name           = 'Default conf';
conf.haveTitle      = 1;
conf.title          = 'title';
conf.interpreterT   = 'none';
conf.xlabel         = 'x-axis';
conf.interpreterX   = 'none';
conf.ylabel         = 'y-axis';
conf.interpreterY   = 'none';
conf.haveLegend     = 0;
conf.legend         = {};
conf.legendLocation = '';
conf.interpreterL   = 'none';
conf.plotLineWidth  = 1;
conf.fontName       = 'Times New Rome';
conf.fontSizeLabel  = 16;
conf.fontSizeTitle  = 16;
conf.fontSizeTick   = 12;
conf.fontSizeLegend = 16;
conf.flagXlim       = 0;
conf.flagYlim       = 0;
conf.xlim           = [];
conf.ylim           = [];
conf.lineStyle      = {
    'k';
};

% valid plotType includes: normal, semilogY, and log
conf.plotType = 'normal';

% Working directory.
conf.workingDirectory = pwd;

% Grid.
conf.flagGridOn = 0;

