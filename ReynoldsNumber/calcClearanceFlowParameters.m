function [aspectRatio, clearanceRatio, Rec, Rea] = calcClearanceFlowParameters(D,C,L,omega,rho,mu,axialFlowRate)
% omega - in rpm
% mu - dynamic viscosity, Pas
% axialFlowRate - in kg/s

aspectRatio    = L/D;
R              = D/2;
clearanceRatio = C/R;
vSurface       = omega/60 * pi * D;
Rec            = rho * vSurface * C / mu;
R2             = R + C;
S              = pi * ( R2^2 - R^2 );
axialVelocity  = axialFlowRate/rho/S;
Rea            = rho * axialVelocity * C / mu;