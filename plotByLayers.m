function plotByLayers(Cx, Cy, conf)
% Cx - the x-axis cell
% Cy - the y-axis cell
% conf - the configuration structure
% 
% the conf structure
% conf.name: the file name for save
% conf.haveTitle: 1 - have title, 0 do not have title
% conf.title: the title
% conf.xlabel: the x axis label
% conf.ylabel: the y axis label
% conf.fontName: the font name
% conf.fontSizeLabel: the font size of the label
% conf.fontSizeTitle: the font size of the title
% conf.fontSizeTick: the font size of the tick on the axis
% conf.haveLegend: 1 - have legend, 0 - no legend
% conf.legend: the legend
% conf.legnedLocation: the location of the legend
% conf.plotLineWidth: the line width of the curve
% conf.flagXlim: 1 - specific xlim, 0 - auto xlim
% conf.flagYlim: 1 - specific ylim, 0 - auto ylim
% conf.xlim
% conf.ylim
% conf.lineStyle
% conf.interpreterX: 'none' or 'Letex'
% conf.interpreterY: 'none' or 'Letex'
% conf.interpreterT: 'none' or 'Letex', for title
% conf.workingDirectory: The directory for saving figure.
%
% Author
% ======
%
% Yaoyu HU <huyaoyu@sjtu.edu.cn>
%
% Date
% ====
%
% Created: 20150818
% 
% Modified: 20160729
% Add working directory.
%
% Modified on 2016-08-02
% Add interpreter for legend. Add grid swith.
% 

nCurves = size(Cx,1);

h = figure('name',conf.name);

set(gca, 'FontSize', conf.fontSizeTick);

if (strcmp(conf.plotType, 'normal') == 1)
    plotFunc = @plot;
elseif (strcmp(conf.plotType, 'semilogy') == 1)
    plotFunc = @semilogy;
elseif (strcmp(conf.plotType, 'log') == 1)
else
    % this is an error
    str = sprintf('The plotType is wrong. Your supplied %s. The following are valid:\n%s, %s, %s',...
        plotType, 'normal', 'semilogy', 'log');
    ex = MException('SJTU:ArgumentError:WrongArgument',str);
    
    throw(ex);
end

plotFunc(Cx{1},Cy{1},conf.lineStyle{1},'LineWidth',conf.plotLineWidth);
hold on
for I = 2:1:nCurves
    plotFunc(Cx{I},Cy{I},conf.lineStyle{I},'LineWidth',conf.plotLineWidth);
end

hold off
if (conf.flagXlim == 1)
    xlim(conf.xlim);
end
if (conf.flagYlim == 1)
    ylim(conf.ylim);
end

if (conf.haveTitle == 1)
    title(conf.title,...
        'Interpreter',conf.interpreterT,...
        'FontSize', conf.fontSizeTitle,...
        'FontName', conf.fontName);
end

xlabel(conf.xlabel,...
    'Interpreter',conf.interpreterX,...
    'FontSize', conf.fontSizeLabel,...
    'FontName', conf.fontName);
ylabel(conf.ylabel,...
    'Interpreter',conf.interpreterY,...
    'FontSize', conf.fontSizeLabel,...
    'FontName', conf.fontName);
if (conf.haveLegend == 1)
    legend(conf.legend,...
        'Location', conf.legendLocation,...
        'Interpreter', conf.interpreterL,...
        'FontSize', conf.fontSizeLegend);
end

if ( conf.flagGridOn == 1 )
    grid on
end

correctedDirectory = strrep(conf.workingDirectory, '\', '/');

saveName = [correctedDirectory, '/', conf.name];

saveas(h,saveName,'fig');
saveas(h,saveName,'png');
saveas(h,saveName,'bmp');
